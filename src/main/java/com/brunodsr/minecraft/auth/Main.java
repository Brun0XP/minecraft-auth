package com.brunodsr.minecraft.auth;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {


    @Override
    public void onEnable() {
        super.onEnable();

        System.out.println("Host:" + System.getenv("POSTGRES_HOST"));
        System.out.println("Port:" + System.getenv("POSTGRES_PORT"));
        System.out.println("Database:" + System.getenv("POSTGRES_DATABASE"));
        System.out.println("User:" + System.getenv("POSTGRES_USERNAME"));
        System.out.println("Password:" + System.getenv("POSTGRES_PASSWORD"));

    }

    @Override
    public void onDisable() {
        super.onDisable();
    }

}
